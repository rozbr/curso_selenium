
from selenium.webdriver import Chrome
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


browser = Chrome(executable_path='./chromedriver')
wait = WebDriverWait(browser, 60)

browser.maximize_window()
browser.get('http://www.portaltransparencia.gov.br/despesas/orgao')

table = wait.until(EC.visibility_of_element_located((By.ID, 'lista')))
ths = table.find_elements_by_xpath('thead/tr/th')

header = []
for th in ths:
    if th.text != 'DETALHAR':
        header.append('"{}"'.format(th.text))

header = ';'.join(header)

arquivo = open('dados.csv', 'w')
arquivo.write(header)
arquivo.write('\n')
arquivo.close()

while True:
    dados = []
    table = wait.until(EC.visibility_of_element_located((By.ID, 'lista')))
    trs = table.find_elements_by_xpath('tbody/tr')
    
    for tr in trs:
        temp = []
        tds = tr.find_elements_by_tag_name('td')
        
        for td in tds[1:]:
            temp.append('"{}"'.format(td.text))

        dados.append(temp)

    arquivo = open('dados.csv', 'a')
    for dado in dados:
        linha = ';'.join(dado)
        arquivo.write(linha)
        arquivo.write('\n')
    arquivo.close()

    link_next_page = browser.find_element_by_id('lista_next')
    link_next_page_classes = link_next_page.get_attribute('class')

    if 'disabled' in link_next_page_classes:
        break

    link_next_page.click()

browser.quit()

